# split v1.1

The folder `split v1.1` contains the split of v1.1 of the corpus with a first round of corrections.

The files `UD.cup` and `WMT.cupt` is a different split of the data in `split v1.1`.
