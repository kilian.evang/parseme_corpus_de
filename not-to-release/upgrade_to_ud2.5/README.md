# Split

In the `split` folder, you can find the results of splitting
`../before_upgrade_to_ud2.5` into two parts (one UD based, with manual
dependency annotations, the other with automatic dependency annotations).

# WMT-alt.cupt

This file contains the results of:
* Re-parsing each sentence in `split/WMT.cupt` with UDPipe
* Using the `to_cupt.py` script to restore the VMWE annotations

Unfortunaltely, `to_cupt.py` discards some MWE annotations due to tokenization
inconsistencies, hence tokenization was finally not updated in the upgrade to
UD2.5 (i.e., sentences were re-parsed with UD, but tokenization was preserved).
