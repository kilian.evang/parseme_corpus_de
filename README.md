# README

This is the README file from the PARSEME verbal multiword expressions (VMWEs) corpus for German, edition 1.3.
See the wiki pages of the [PARSEME corpora](https://gitlab.com/parseme/corpora/-/wikis/) initiative for the full documentation of the annotation principles.

The present German data result from an update and an extension of the German part of the [PARSEME 1.2 corpus](http://hdl.handle.net/11234/1-3367).
For the changes with respect to the 1.2 version, see the change log below.

## Corpora

VMWEs have been annotated in the following corpora:

- `WMT.cupt`:
   - sentences 1–7500 from the 2014 edition of the annual Workshop on Statistical Machine Translation, the [WMT 2014 News Crawl: articles from 2013](http://statmt.org/wmt14/translation-task.html#download) (Bojar et al. 2014) 
   - sentence IDs: `newscrawl-1` to `newscrawl-7500`
   - text genre: news
- `UD.cupt`:
   - sentences `train-s1501` to `train-s3000` from v2.5 of the [German part](https://github.com/UniversalDependencies/UD_German-GSD) of the [Universal Dependencies (UD) corpus](http://universaldependencies.org).
   - sentence IDs: `train-s1501` to `train-s3000`
   - text genre: news, reviews, wikis

The raw data is drawn from the same WMT corpus as above, using sentences 1,000,001 up to 11,000,000.

## Provided annotations

The VMWE-annotated data are in the [.cupt](http://multiword.sourceforge.net/cupt-format) format and the raw corpus in the [.conllu](https://universaldependencies.org/format.html) format. Here is detailed information about some columns:

* LEMMA (column 3): Available. Automatically annotated (UDPipe).
* UPOS (column 4): Available. Automatically annotated for `WMT.cupt` (UDPipe), manually annotated for `UD.cupt`.
* XPOS (column 5): Available. Automatically annotated for `WMT.cupt` (UDPipe), automatically annotated for `UD.cupt` (TreeTagger).
* FEATS (column 6): Available. Automatically annotated (UDPipe).
* HEAD and DEPREL (columns 7 and 8): Available. Automatically annotated for `WMT.cupt`, manually annotated for `UD.cupt`. The inventory is [Universal Dependency Relations](http://universaldependencies.org/u/dep).
* MISC (column 10): No-space information available. Automatically annotated.
* PARSEME:MWE (column 11): Manually annotated for `WMT.cupt` and `UD.cupt`. The following [VMWE categories](http://parsemefr.lif.univ-mrs.fr/parseme-st-guidelines/1.1/?page=030_Categories_of_VMWEs) are annotated: IRV, LVC.cause, LVC.full, VID, VPC.full, VPC.semi.

Automated annotation in both the annotated and the raw corpus was performed using [UDPipe 2 with model german-gsd-ud-2.10-220711](https://ufal.mff.cuni.cz/udpipe/2/models).

## Statistics
To know the number of annotated VMWEs of different types and with different properties (length, continuity, etc.), use these scripts: [mwe-stats.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats.py) and [mwe-stats-simple.py](https://gitlab.com/parseme/utilities/-/blob/master/st-organizers/corpus-statistics/mwe-stats-simple.py). 

## Tokenization

- `WMT.cupt` & raw data: the sentence tokenization was performed using the WMT script for German.

- `UD.cupt` & raw data: the word tokenization is that of German UD treebanks with some preposition-determiner contractions being analysed , e.g., *zum* --> *zu dem*, *im* --> *in dem* etc.

- `WMT.cupt`: the word tokenization does not analyse preposition-determiner contractions.

## Annotation authors

Building on the VMWE annotations made by Fabienne Cap and Glorianna Jagfeld in v1.0, and the VMWE annotations added and refined by Timm Lichte and Rafael Ehren in v1.1, further corrections have been conducted by Timm Lichte and Rafel Ehren in v1.2.

## License

All VMWEs annotations are distributed under the terms of the [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/) license.
The lemmas, POS-tags, morphological features and dependency tags (contained in the CoNLL-U files) are distributed under the [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0) license, i.e. the same license as the [German Universal Dependencies data](http://universaldependencies.org/#de), on which [UDPipe](https://ufal.mff.cuni.cz/udpipe) was trained. All sentences are distributed under the [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0) license.


## Contact

- timm.lichte@uni-tuebingen.de
- rafael.ehren@hhu.de

## References

Bojar, Ondřej, Christian Buck, Christian Federmann, Barry Haddow, Philipp Koehn, Johannes Leveling, Christof Monz, Pavel Pecina, Matt Post, Herve Saint-Amand, Radu Soricut, Lucia Specia, Aleš Tamchyna. 2014. Findings of the 2014 Conference on Machine Translation. In Proceedings of the Ninth Workshop on Statistical Machine Translation, 12–58. Baltimore, MD: Association for Computational Linguistics. http://statmt.org/wmt14/pdf/W14-3302.pdf

## Change log

- **2023-04-15**:
  - Version 1.3 of the corpus was released on LINDAT.
  - The sentence order in UD.cupt was manually restored to fit the source data from the German UD treebank (de_gsd-ud-train.conllu). The previous order of identifiers was: 1678-1868; 1501-1677; 1869-3000. It was changed to 1501-3000.
  - The morphosyntactic annotation (columns UPOS to MISC) was updated by Agata Savary:
    - For UD.cupt is was synchronised with the German UD corpus [GSD](https://github.com/UniversalDependencies/UD_German-GSD)
    - For WMT.cupt is was regenerated with UDPipe 2 model german-gsd-ud-2.10-220711
- **2020-07-09**:
  - [Version 1.2](http://hdl.handle.net/11234/1-3367) of the corpus was released on LINDAT.
- **2018-04-30**:
  - [Version 1.1](http://hdl.handle.net/11372/LRT-2842) of the corpus was released on LINDAT.
- **2017-01-20**:
  - [Version 1.0](http://hdl.handle.net/11372/LRT-2282) of the corpus was released on LINDAT.  

